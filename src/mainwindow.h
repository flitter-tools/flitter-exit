#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStyle>
#include <QDesktopWidget>
#include <QProcess>
#include <QMessageBox>

#include "xutils.h"

namespace Ui {class MainWindow;}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

    private slots:
        void on_btn_shutdown_clicked();
        void on_btn_reboot_clicked();
        void on_btn_suspend_clicked();
        void on_btn_logout_clicked();

    private:
        Ui::MainWindow *ui;

        void quitMessage(const QString &procstr);
};

#endif // MAINWINDOW_H

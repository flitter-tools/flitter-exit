#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <QFrame>
#include <QDesktopWidget>
#include <QMouseEvent>

namespace Ui {class fbackground;}

class fbackground : public QFrame
{
    Q_OBJECT

    public:
        explicit fbackground(QWidget *parent = 0);
        ~fbackground();

    protected:
        void mousePressEvent(QMouseEvent *event) override;

    private:
        Ui::fbackground *ui;
};

#endif // BACKGROUND_H

#include "mainwindow.h"
#include "background.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    fbackground bga;
    bga.show();

    MainWindow w;
    w.show();

    return a.exec();
}

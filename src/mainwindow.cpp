#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent, Qt::Window | Qt::WindowStaysOnTopHint | Qt::X11BypassWindowManagerHint),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::quitMessage(const QString &procstr)
{
    if (xutils::lws() > 1)
    {
        QMessageBox msg;
        msg.setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint | Qt::X11BypassWindowManagerHint);
        msg.setText("You still have " + QString::number(xutils::lws()) + " window(s) active. Want to drop it all?");
        msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);

        int ret = msg.exec();

        if (QMessageBox::Ok == ret)
        {
            QProcess proc;
            proc.startDetached(procstr);
            qApp->quit();
        }
        else
        {
            qApp->quit();
        }
    }
    else
    {
        QProcess proc;
        proc.startDetached(procstr);
        qApp->quit();
    }
}

void MainWindow::on_btn_shutdown_clicked()
{
    quitMessage("sudo shutdown -h now");
}

void MainWindow::on_btn_reboot_clicked()
{
    quitMessage("sudo reboot");
}

void MainWindow::on_btn_suspend_clicked()
{
    quitMessage("sudo pm-suspend");
}

void MainWindow::on_btn_logout_clicked()
{
    quitMessage("openbox --exit");
}

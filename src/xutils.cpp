#include "xutils.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>

xutils::xutils(){}

unsigned int xutils::lws()
{
    Display *dpy = XOpenDisplay(0);
    XInternAtom(dpy, "_NET_WM_NAME", 0);

    unsigned int n;
    unsigned int c = 1; /* count start as 1, yes */
    Window win = DefaultRootWindow(dpy);
    Window *wins, *w, dw;
    XWindowAttributes wa;

    if(!XQueryTree(dpy, win, &dw, &dw, &wins, &n))
        return 0;

    for(w = &wins[n-1]; w >= &wins[0]; w--)
        if(XGetWindowAttributes(dpy, *w, &wa)
        && !wa.override_redirect && wa.map_state == IsViewable)
            c++;

    XFree(wins);

    return c-1; /* ignore tint2 */
}

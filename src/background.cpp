#include "background.h"
#include "ui_background.h"

fbackground::fbackground(QWidget *parent) :
    QFrame(parent, Qt::Window | Qt::X11BypassWindowManagerHint),
    ui(new Ui::fbackground)
{
    ui->setupUi(this);

    const int scr_width = QApplication::desktop()->width();
    const int scr_height = QApplication::desktop()->height();
    this->setGeometry( 0, 0, scr_width, scr_height );
}

fbackground::~fbackground()
{
    delete ui;
}

void fbackground::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        qApp->quit();
    }
}
